﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Overseer.Web.Common
{
    public class CachedFramesQueue
    {
        private LinkedList<FrameQueueItem> frames = new LinkedList<FrameQueueItem>();
        private readonly object queueLocker;
        public CachedFramesQueue(object locker)
        {
            this.queueLocker = locker;
        }

        public void Enqueue(byte[] frame)
        {
            lock(this.queueLocker)
                frames.AddLast(new FrameQueueItem() {Frame = frame });
        }

        public void IncrementStatic()
        {
            lock(this.queueLocker)
                this.frames.First.Value.Duration++;
        }

        public bool IsEmpty()
        {
            lock (this.queueLocker) 
                return this.frames.First.Value.Duration == 0;
        }

        public byte[] Dequeue()
        {
            lock (this.queueLocker)
            {
                if (this.frames.First.Value.Duration == 0)
                    return null;

                FrameQueueItem returnItem = this.frames.First.Value;
                if (returnItem.Duration == 1 && this.frames.Count > 1)
                    this.frames.RemoveFirst();
                else returnItem.Duration--;

                return returnItem.Frame;
            }

        }
    }
}
