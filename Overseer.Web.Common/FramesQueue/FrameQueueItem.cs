﻿using Overseer.DbContext;

namespace Overseer.Web.Common
{
    public class FrameQueueItem
    {
        public byte[] Frame { get; set; }
        /// <summary>
        /// Static frames have duration > 1
        /// </summary>
        public uint Duration { get; set; } = 1;
    }

    public static class CacheUtilities
    {
        /// <summary>
        /// Uniform method to get a cache key. Use it in order not to mix up the order of tuples values
        /// </summary>
        /// <param name="m"></param>
        /// <param name="d"></param>
        /// <returns>Item1 - machine id, Item2 - device id</returns>
        public static (int, int) GetCacheKey(Machine m, OutputDevice d) => (m.Id, d.Id);
    }
}
