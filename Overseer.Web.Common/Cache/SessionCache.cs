﻿using Overseer.DbContext;
using System.Threading;
using System.Threading.Tasks;

namespace Overseer.Web.Common.Cache
{
    public class SessionCache
    {
        private object locker = new object();
        public Session RunningSession { get; private set; }
        public CachedFramesQueue FramesQueue { get; private set; }
        public CancellationTokenSource CancelTrigger { get; private set; }
        public SessionCache(Session s, CancellationTokenSource cts)
        {
            this.FramesQueue = new CachedFramesQueue(locker);
            this.CancelTrigger = cts;
            this.RunningSession = s;
        }
    }
}
