﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Certificate;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.IISIntegration;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using OverseerAPI.Services;

namespace OverseerAPI
{
    //configuring certificate guideline: https://docs.microsoft.com/en-us/aspnet/core/security/authentication/certauth?view=aspnetcore-3.1
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            string connection = Configuration.GetConnectionString("DefaultConnection");

            services.AddControllers();
            services.AddTransient<CertificateValidationService>();//not sure if this is needed
            services.AddDbContext<Overseer.DbContext.OverseerDbContext>(options => options.UseSqlServer(connection));
            services.AddTransient<UserService>();
            services.AddTransient<TokenService>();

            services.AddAuthentication(CertificateAuthenticationDefaults.AuthenticationScheme).AddCertificate(options => {
                options.AllowedCertificateTypes = CertificateTypes.All; //default is chained
                options.ValidateCertificateUse = false;//this is true by default, but i'm not sure if my test cert issued by local iis will make a suitable cert for this prop
                options.RevocationMode = System.Security.Cryptography.X509Certificates.X509RevocationMode.NoCheck;
                options.ValidateValidityPeriod = true;
                options.Events = new CertificateAuthenticationEvents()
                {
                    OnCertificateValidated = context =>
                    {
#if DEBUG
                        Stopwatch sw = new Stopwatch();
                        sw.Start();
#endif
                        try
                        {
                            Console.WriteLine($"{DateTime.Now.ToShortTimeString()} on cert validated event fired");
                            var validationService = context.HttpContext.RequestServices.GetService<CertificateValidationService>();//dayum boi
                            string remoteHostname = Dns.GetHostEntry(context.HttpContext.Connection.RemoteIpAddress).HostName;
                            if (validationService.ValidateCertificate(context.ClientCertificate, remoteHostname))
                            {
                                Console.WriteLine($"{DateTime.Now.ToShortTimeString()} yeet");
                                //бля вот по добру бы не проводить аутентификацию на каждый кадр, а то это ебануться оверхеды. надо как то клеймами научить залупу оперировать

                                var claims = new[]
                                {
                                    new Claim(ClaimTypes.NameIdentifier, context.ClientCertificate.Subject, ClaimValueTypes.String, context.Options.ClaimsIssuer),
                                    new Claim(ClaimTypes.Name, context.ClientCertificate.Subject, ClaimValueTypes.String, context.Options.ClaimsIssuer)
                                };

                                context.Principal = new ClaimsPrincipal(new ClaimsIdentity(claims, context.Scheme.Name));

                                context.Success();
                            }
                            else
                            {
                                Console.WriteLine($"{DateTime.Now.ToShortTimeString()} fuck");
                                context.Fail("User with this certificate is unknown to the system");
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine($"{DateTime.Now.ToShortTimeString()} ai fakkullo");
                            context.Fail("sheeeit");
                        }
#if DEBUG
                        sw.Stop();
                        Console.WriteLine($"cert auth ms elapsed: {sw.ElapsedMilliseconds}");
#endif
                        return Task.CompletedTask;
                    }
                };
            });

            //https://damienbod.com/2019/06/13/certificate-authentication-in-asp-net-core-3-0/
            services.AddCertificateForwarding(options =>
            {
                options.CertificateHeader = "X-ARR-ClientCert";
                options.HeaderConverter = (headerValue) =>
                {
                    X509Certificate2 clientCertificate = null;
                    if (!string.IsNullOrWhiteSpace(headerValue))
                    {
                        byte[] bytes = CommonUtilities.Utilities.StringToByteArray(headerValue);
                        clientCertificate = new X509Certificate2(bytes);
                    }

                    return clientCertificate;
                };
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseRouting();

            app.UseCertificateForwarding();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
