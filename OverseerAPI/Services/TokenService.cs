﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Overseer.CommonUtilities;
using Overseer.DbContext;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace OverseerAPI.Services
{
    public class TokenService
    {
        private readonly IConfiguration config;

        public TokenService(IConfiguration config)
        {
            this.config = config;
        }

        public OverseerToken GenerateToken(Client2Machine c2m)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            byte[] key = Encoding.ASCII.GetBytes(config.GetValue<string>("JwtServerKey"));
            int lifetime = config.GetValue<int>("JWTLifetimeSec");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, c2m.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddSeconds(lifetime),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return new OverseerToken()
            {
                AccessToken = tokenHandler.WriteToken(token),
                LifetimeSeconds = lifetime
            };
        }

        private string generateRefreshToken()
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }
    }
}
