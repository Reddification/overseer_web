﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Overseer.DbContext;

namespace OverseerAPI.Services
{
    public class UserService
    {
        private readonly Overseer.DbContext.OverseerDbContext dbContext;

        public UserService(Overseer.DbContext.OverseerDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public Client2Machine GetClient2MachineFromRequest(HttpContext httpContext)
        {
            Client client = dbContext.Clients.Single(s => s.CertThumbprint == httpContext.Connection.ClientCertificate.Thumbprint);
            IPHostEntry requestor = Dns.GetHostEntry(httpContext.Connection.RemoteIpAddress);
            Machine machine = dbContext.Machines.FirstOrDefault(f => f.DomainName == requestor.HostName);// so far machines identificator is its name in domain
            Client2Machine c2m = dbContext.Client2Machines.FirstOrDefault(f => f.ClientId == client.Id && f.MachineId == machine.Id);
            return c2m;
        }
    }
}
