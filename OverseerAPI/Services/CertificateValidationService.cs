﻿using Overseer.DbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace OverseerAPI.Services
{
    //https://docs.microsoft.com/en-us/aspnet/core/security/authentication/certauth?view=aspnetcore-3.1#configure-your-host-to-require-certificates
    public class CertificateValidationService
    {
        private readonly Overseer.DbContext.OverseerDbContext dbContext;

        public CertificateValidationService(Overseer.DbContext.OverseerDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public bool ValidateCertificate(X509Certificate2 clientCertificate, string remoteHostname)
        {
            IEnumerable<Client2Machine> c2ms = dbContext.Client2Machines.Where(f => f.Machine.DomainName == remoteHostname);
            if (!c2ms.Any())
                return false;

            return c2ms.Where(s => s.Client.CertThumbprint == clientCertificate.Thumbprint).Count() == 1;
        }
    }
}
