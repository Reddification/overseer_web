﻿$rootcert = ( Get-ChildItem -Path cert:\LocalMachine\My\"BBE0AE077BEEB78C685D7A4B66D4C18FF3C71DC0" )

#New-SelfSignedCertificate -certstorelocation cert:\localmachine\my -dnsname "localhost" -Signer $rootcert -NotAfter (Get-Date).AddYears(20) -FriendlyName "overseerClient"

$mypwd = ConvertTo-SecureString -String "1234" -Force -AsPlainText

Get-ChildItem -Path cert:\localMachine\my\"672EACB4E89061F3068068DFF76BF5F1FBFB0E94" | Export-PfxCertificate -FilePath C:\certs\client_from_root.pfx -Password $mypwd

Export-Certificate -Cert cert:\localMachine\my\"672EACB4E89061F3068068DFF76BF5F1FBFB0E94" -FilePath C:\certs\client_from_root.crt