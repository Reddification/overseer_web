﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using OverseerAPI.Services;

namespace OverseerAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    //https://jasonwatmore.com/post/2019/10/11/aspnet-core-3-jwt-authentication-tutorial-with-example-api
    public class AuthController : ControllerBase
    {
        private readonly IConfiguration config;
        private readonly UserService userService;
        private readonly TokenService tokenService;

        public AuthController(IConfiguration config, UserService userService, TokenService tokenService)
        {
            this.config = config;
            this.userService = userService;
            this.tokenService = tokenService;
        }

        [HttpPost]
        public IActionResult Authenticate()
        {
            return new JsonResult(tokenService.GenerateToken(userService.GetClient2MachineFromRequest(HttpContext)));
        }
    }
}