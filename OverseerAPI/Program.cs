using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Https;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using OverseerAPI.Services;

namespace OverseerAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    X509Certificate2 serverCert = new X509Certificate2(Path.Combine("C:/certs", "overseerServer.pfx"), "1234");
                    webBuilder.UseKestrel(o =>
                    {
                        o.ConfigureHttpsDefaults(a =>
                        {
                            a.ClientCertificateMode = ClientCertificateMode.RequireCertificate;
                            a.ServerCertificate = serverCert;
                            //a.AllowAnyClientCertificate();//probably should be used only in test scenarios
                        });

                    });// not sure if this is needed
                    //https://docs.microsoft.com/en-us/aspnet/core/security/authentication/certauth?view=aspnetcore-3.1
                    //webBuilder.ConfigureKestrel(o =>
                    //{
                    //    o.ConfigureHttpsDefaults(a => a.ClientCertificateMode = ClientCertificateMode.RequireCertificate);
                    //});
                });
            //.ConfigureServices(services => services.AddHostedService<FramesProcessorService>()); //https://docs.microsoft.com/en-us/aspnet/core/fundamentals/host/hosted-services?view=aspnetcore-3.1&tabs=visual-studio
    }
}
