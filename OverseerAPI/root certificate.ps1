﻿#https://docs.microsoft.com/en-us/aspnet/core/security/authentication/certauth?view=aspnetcore-3.1#create-certificates-in-powershell

#New-SelfSignedCertificate -DnsName "localhost", "overseerServer" -CertStoreLocation "cert:\LocalMachine\My" -NotAfter (Get-Date).AddYears(20) -FriendlyName "overseerServer" -KeyUsageProperty All -KeyUsage CertSign, CRLSign, DigitalSignature

$mypwd = ConvertTo-SecureString -String "1234" -Force -AsPlainText

Get-ChildItem -Path cert:\localMachine\my\"BBE0AE077BEEB78C685D7A4B66D4C18FF3C71DC0" | Export-PfxCertificate -FilePath C:\certs\overseerServer.pfx -Password $mypwd

Export-Certificate -Cert cert:\localMachine\my\"BBE0AE077BEEB78C685D7A4B66D4C18FF3C71DC0" -FilePath C:\certs\overseerServer.crt