﻿using System;

namespace CommonUtilities
{
    /// <summary>
    /// Tells the server what kind of message is it
    /// </summary>
    public enum FrameControl
    {
        /// <summary>
        /// i.e. when user's OS shuts down
        /// </summary>
        Stop = 0,
        /// <summary>
        /// New frame when screen content actually changed
        /// </summary>
        New = 1,
        /// <summary>
        /// When screen content hasn't changed there's no need to send the same frame again, so just let the server know to use previous, cached frame
        /// </summary>
        Static = 2
    }

    public class Frame
    {
        /// <summary>
        /// Frame itself
        /// </summary>
        public byte[] Data { get; set; }
        /// <summary>
        /// Device/adapter name
        /// </summary>
        public string SourceId { get; set; }
        public FrameControl Control { get; set; }
    }
}
