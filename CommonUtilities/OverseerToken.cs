﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Overseer.CommonUtilities
{
    public class OverseerToken
    {
        public string AccessToken { get; set; }
        public int LifetimeSeconds { get; set; }
    }
}
