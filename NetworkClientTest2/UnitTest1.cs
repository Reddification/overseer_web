﻿using System;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetworkClientTest2.Properties;
using Overseer.NetworkClient;

namespace NetworkClientTest2
{
    [TestClass]
    public class UnitTest1
    {
        private byte[] frame1;
        private byte[] frame2;
        private string sourceId1 = "NVIDIA";
        private string sourceId2 = "INTEL";

        private OverseerNetworkClient nClient;
        [TestInitialize]
        public void Setup()
        {
            //in theory we should also start the local web api service here. smth like new Process("../OverseerAPI/debug/OverseerAPI.exe").Start();
            //but so far i'm doing it manually since i'm debugging both projects
            nClient = new OverseerNetworkClient("https://localhost:5001/", "https://localhost:44314/", "overseerClient");
            if (!nClient.Authenticate())
                Console.WriteLine("fuck shit");
            using (var ms = new MemoryStream())
            {
                Resources.KRYSA.Save(ms, ImageFormat.Png);
                frame1 = ms.ToArray();
            }

            using (var ms = new MemoryStream())
            {
                Resources.RYZAR.Save(ms, ImageFormat.Jpeg);
                frame2 = ms.ToArray();
            }
        }

        [TestMethod]
        public void TestSendFrame()
        {
            var res = nClient.SendNewFrame(frame1, sourceId1);
            Assert.AreEqual(HttpStatusCode.OK, res);
        }

        [TestMethod]
        public void TestMultipleFrames()
        {
            HttpStatusCode res1 = nClient.SendNewFrame(frame1, sourceId1);
            HttpStatusCode res2 = nClient.SendNewFrame(frame1, sourceId2);
            HttpStatusCode res3 = nClient.SendStaticFrame(sourceId1);
            HttpStatusCode res4 = nClient.SendNewFrame(frame2, sourceId2);
            HttpStatusCode res5 = nClient.SendStaticFrame(sourceId2);
            HttpStatusCode res6 = nClient.SendNewFrame(frame1, sourceId1);
            HttpStatusCode res7 = nClient.SendStaticFrame(sourceId2);
            HttpStatusCode res8 = nClient.SendStaticFrame(sourceId2);
            HttpStatusCode res9 = nClient.SendStaticFrame(sourceId2);

            Assert.IsTrue(res1 == HttpStatusCode.OK && res1 == res2 && res2 == res3 && res3 == res4 && res4 == res5 && res5 == res6 && res6 == res7 && res7 == res8 && res8 == res9);
        }

        [TestMethod]
        public void TempTest()
        {
            nClient.Test();
            nClient.Test();
            nClient.Test();
            nClient.Test();
            nClient.Test();
            nClient.Test();
            nClient.Test();
            nClient.Test();
            nClient.Test();
            nClient.Test();
            nClient.Test();
            nClient.Test();
            nClient.Test();
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void TestAuth()
        {
            bool authSuccess = nClient.Authenticate();
            Assert.IsTrue(authSuccess);
        }
    }
}
