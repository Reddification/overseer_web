﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using CommonUtilities;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using Overseer.DbContext;
using Overseer.Web.Common;
using Overseer.Web.Common.Cache;
using OverseerWeb.Services;

namespace OverseerWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [RequireHttps]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    //TODO: sessions management logic should be moved to a single service and injected to both controller and the hosted service because right now it's sharded too much
    public class FramesController : ControllerBase
    {
        private readonly Overseer.DbContext.OverseerDbContext dbContext;

        private readonly SessionCacheManagementService scmService;

        public FramesController(OverseerDbContext dbContext, SessionCacheManagementService scmService)
        {
            this.dbContext = dbContext;
            this.scmService = scmService;
        }

        [HttpPost]
        public IActionResult ReceiveFrame(Frame frame)
        {
            //1. first check if client is legit
            int c2mId = Convert.ToInt32(this.HttpContext.User.Identity.Name);
            Client2Machine c2m = dbContext.Client2Machines.FirstOrDefault(f => f.Id == c2mId);
            if (c2m == null)
                return Unauthorized();
            //2. then get some relevant objects
            OutputDevice oDev = c2m.Machine.OutputDevices.FirstOrDefault(f => f.Name == frame.SourceId); //should not be null
            //to safe some network resources we keep the last frame from each client for each of his output devices so that in case the screen content didn't change we could just append previous frame that we already have
            (int, int) sessionCacheKey = scmService.GetCacheKey(c2m.Machine, oDev);
            SessionCache cf = this.scmService.GetCachedSession(sessionCacheKey);

            Session cSession;

            if (cf != null)
                cSession = cf.RunningSession;
            else //else cache has either been flushed or this device is used for the first time in this session
            {
                killLostSessions(c2m.MachineId, oDev.Id);
                //running session timed out since it's record is no longer in cache so we declare it finished
                cSession = new Session()
                {
                    DeviceId = oDev.Id,
                    StartTime = DateTime.Now,
                    FileId = Guid.NewGuid(),
                    Client2MachineId = c2m.Id
                };


                cf = scmService.CacheSession(cSession, sessionCacheKey);

                dbContext.Sessions.Add(cSession);
                dbContext.SaveChanges();
            }

            //3. Process the frame message
            switch (frame.Control)
            {
                case FrameControl.Stop:
                    if (cf != null)
                    {
                        scmService.KillSession(dbContext, sessionCacheKey.Item1, sessionCacheKey.Item2);
                        cf.CancelTrigger.Cancel();
                    }
                    break;
                case FrameControl.New:
                    cf.FramesQueue.Enqueue(frame.Data);
                    break;
                case FrameControl.Static:
                    cf.FramesQueue.IncrementStatic();
                    break;
                default:
                    break;
            }

            return Ok();
        }

        //if session hasn't been closed correctly (i.e. app crashed an oncacheevicted never triggered) - kill them all
        //to be honest, this is a dirty hack. either sessions should be reconsidered or they should never be lost
        private void killLostSessions(int machineId, int deviceId)
        {
            foreach (var s in dbContext.Sessions.Where(w => !w.EndTime.HasValue && w.Client2Machine.MachineId == machineId && w.DeviceId == deviceId).OrderByDescending(ob => ob.StartTime))
                s.EndTime = DateTime.Now;

            dbContext.SaveChanges();
        }
    }
}