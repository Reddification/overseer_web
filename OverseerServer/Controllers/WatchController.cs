﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Overseer.DbContext;
using OverseerWeb.ViewModels;

namespace OverseerWeb.Controllers
{
    [RequireHttps]
    public class WatchController: Controller
    {
        private readonly OverseerDbContext dbContext;
        private readonly string filesFolder;
        public WatchController(OverseerDbContext dbContext, IConfiguration config)
        {
            this.dbContext = dbContext;
            this.filesFolder = config.GetValue<String>("VideosLocation");
        }

        [HttpGet]
        public IActionResult GetFile(string fileId)
        {
            //if (User.Claims.Contains())
            if (this.HttpContext.User.Identity.IsAuthenticated)
            {
                PortalUser user = dbContext.Users.FirstOrDefault(f => f.Name == this.HttpContext.User.Identity.Name);
                if ((user?.Roles & Role.Admin) > 0)
                    return File(getRenderedFile(fileId), "video/mp4");
            }

            return Unauthorized();
        }

        private byte[] getRenderedFile(string fileId)
        {
            return System.IO.File.ReadAllBytes(filesFolder + "/" + fileId);
        }
    }
}
