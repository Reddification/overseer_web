﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OverseerWeb.ViewModels;

namespace OverseerWeb.Controllers
{
    [RequireHttps]
    [AllowAnonymous]
    public class HomeController : Controller
    {
        private readonly Overseer.DbContext.OverseerDbContext dbContext;
        public HomeController(Overseer.DbContext.OverseerDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public IActionResult Index()
        {
            HomeViewmodel hvm = new HomeViewmodel()
            {
                Filter = OverseerFilter.Client
            };

            return View(hvm);
        }

        #region overview filters: by client, pool or machine

        [HttpGet]
        public JsonResult GetRelevantClients()
        {
            //return all clients that have finished segments - videos that can be watched
            return new JsonResult(dbContext.Clients.Where(w => dbContext.Sessions.Any(s => s.EndTime.HasValue && s.Client2Machine.ClientId == w.Id))
                                                   .Select(s => new ClientViewmodel() 
                                                    { 
                                                        Id = s.Id,
                                                        Name = s.Name
                                                    }));
        }


        [HttpGet]
        public JsonResult GetRelevantMachines()
        {
            return new JsonResult(dbContext.Machines.Where(w => dbContext.Sessions.Any(s => s.EndTime.HasValue && s.Client2Machine.MachineId == w.Id))
                                                 .Select(s => new MachineViewmodel()
                                                 {
                                                     Id = s.Id,
                                                     Name = s.DomainName,
                                                 }));
        }

        #endregion

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
