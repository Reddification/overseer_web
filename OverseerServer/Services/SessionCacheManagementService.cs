﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Primitives;
using Overseer.DbContext;
using Overseer.Web.Common.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace OverseerWeb.Services
{
    public class SessionCacheManagementService
    {
        private readonly IMemoryCache memoryCache;
        private const int cacheLifetime = 60;//in seconds

        public SessionCacheManagementService(IMemoryCache memoryCache)
        {
            this.memoryCache = memoryCache;
        }

        public void KillSession(OverseerDbContext dbContext, int machineId, int devId)
        {
            Session finishedSession = dbContext.Sessions.FirstOrDefault(f => !f.EndTime.HasValue && f.Client2Machine.MachineId == machineId && f.DeviceId == devId);
            finishedSession.EndTime = DateTime.Now;
            dbContext.SaveChanges();
        }

        //in theory this will update DB once session cache is expired
        private void onCacheEvicted(object key, object value, EvictionReason reason, object state)
        {
            //sweet https://thomaslevesque.com/2016/07/25/tuples-in-c-7/
            //1st item is machine id, 2nd - device id
            (int, int) cacheKey = ((int, int))key;
            SessionCache cf = (SessionCache)value;
            cf.CancelTrigger.Cancel();
        }

        public SessionCache CacheSession(Session cSession, (int, int) sessionCacheKey)
        {
            var memEvictionOptions = new MemoryCacheEntryOptions() { SlidingExpiration = TimeSpan.FromSeconds(cacheLifetime) };

            //https://stackoverflow.com/a/47949111/7162511
            var expirationToken = new CancellationChangeToken(new CancellationTokenSource(TimeSpan.FromSeconds(cacheLifetime)).Token);
            memEvictionOptions.AddExpirationToken(expirationToken);

            memEvictionOptions.RegisterPostEvictionCallback(onCacheEvicted);
            var cf = new SessionCache(cSession, new CancellationTokenSource());

            this.memoryCache.Set<SessionCache>(sessionCacheKey, cf, memEvictionOptions);
            return cf;
        }

        public (int, int) GetCacheKey(Machine m, OutputDevice d) => (m.Id, d.Id);

        public SessionCache GetCachedSession((int, int) sessionCacheKey)
        {
            SessionCache cf = null;
            this.memoryCache.TryGetValue<SessionCache>(sessionCacheKey, out cf);
            return cf;
        }
    }
}
