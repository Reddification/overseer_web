﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using Overseer.Web.Common;
using Overseer.DbContext;
using Overseer.Web.Common.Cache;

namespace OverseerWeb.Services
{
    public class FramesProcessorService : IHostedService
    {
        private readonly IServiceScopeFactory serviceScopeFactory;
        private readonly string filesFolder;
        private List<FrameRecordingState> runningFrameWritings = new List<FrameRecordingState>();
        private object runningWritingsLocker = new object();
        public FramesProcessorService(IServiceScopeFactory ssf, IConfiguration configuration)
        {
            this.serviceScopeFactory = ssf;
            this.filesFolder = configuration.GetValue<string>("VideosLocation");
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            return Task.Run(() => 
            {
                //check for new queues every 20 seconds
                while (true)
                {
                    //this scope is required for dbcontext. otherwise shit will crush: https://stackoverflow.com/a/48368934/7162511
                    using (var scope = serviceScopeFactory.CreateScope())
                    {
                        OverseerDbContext dbContext = scope.ServiceProvider.GetService<Overseer.DbContext.OverseerDbContext>();
                        SessionCacheManagementService scmService = scope.ServiceProvider.GetService<SessionCacheManagementService>();
                        //foreach running session which frames are still not being written
                        foreach (Session unprocessedSession in dbContext.Sessions.Where(w => !w.EndTime.HasValue).AsEnumerable().Where(w => !this.runningFrameWritings.Any(a => a.SessionId == w.Id)))
                        {
                            //get cache key
                            (int, int) cacheKey = CacheUtilities.GetCacheKey(unprocessedSession.Client2Machine.Machine, unprocessedSession.Device);
                            SessionCache cFrames = scmService.GetCachedSession(cacheKey);
                            //try getting the cached frames
                            if (cFrames != null)
                            {
                                this.runningFrameWritings.Add(new FrameRecordingState() { SessionId = unprocessedSession.Id });
                                //await processSession(cFrames, unprocessedSession.FileId, unprocessedSession.Id);
                                //TODO: finalizing the session in the cache evcition callback doesnt seem to be working. perhaps i should add a cancellation token callback for this shit here
                                Task.Run(() => processSession(cFrames, unprocessedSession.FileId, unprocessedSession.Id)).ContinueWith((t) => killSession(cacheKey.Item1, cacheKey.Item2)); //start appending frames
                            }
                        }
                    }

                    Thread.Sleep(20000);
                }
            });
        }

        private void killSession(int machineId, int devId)
        {
            using (var scope = serviceScopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetService<OverseerDbContext>();
                SessionCacheManagementService scmService = scope.ServiceProvider.GetService<SessionCacheManagementService>();
                scmService.KillSession(dbContext, machineId, devId);
            }
        }

        private void processSession(SessionCache sessionCache, Guid fileId, int SessionId)
        {
            int flushCounter = 0;
            //you should always leave one frame to be able to use it as static
            while(!sessionCache.CancelTrigger.IsCancellationRequested)
            {
                if (!sessionCache.FramesQueue.IsEmpty())
                {
                    byte[] f = sessionCache.FramesQueue.Dequeue();
                    ProcessFrame(f, fileId);
                    if (++flushCounter > 30 * 60)
                    {
                        FlushToDisk((this.filesFolder + "/" + fileId.ToString()).ToCharArray());
                        flushCounter = 0;
                    }
                }
                //if there are no frames then fuck it chill for a second
                else Thread.Sleep(1000);
            }

            lock (runningWritingsLocker)
                this.runningFrameWritings.Remove(runningFrameWritings.FirstOrDefault(f => f.SessionId == SessionId));
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.Run(() => { }); //stub
        }

        public static void ProcessFrame(byte[] frame, Guid fileId)
        {
            using (BinaryWriter bw = new BinaryWriter(new FileStream($"S:/overseertest/{fileId}", FileMode.OpenOrCreate, FileAccess.ReadWrite)))
            {
                bw.Write(frame);
                bw.Flush();
            }
        }

        public static void FlushToDisk(char[] v)
        {
            //this is just a stub
        }

        //[DllImport("FramesProcessor.dll")]
        //public static extern int AppendFrame();
        //[DllImport("FramesProcessor.dll")]
        //public static extern int FlushToDisk(char[] filename);
        //[DllImport("FramesProcessor.dll")]
        //public static extern bool ProcessFrame(byte[] frame, string sourceId);
        //[DllImport("FramesProcessor.dll")]
        //public static extern bool ProcessStaticFrame(string sourceId);

    }
}
