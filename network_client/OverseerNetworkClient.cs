﻿using CommonUtilities;
using Newtonsoft.Json;
using Overseer.CommonUtilities;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Overseer.NetworkClient
{
    public class OverseerNetworkClient:IDisposable
    {
        private HttpClient framesHttpClient;
        private readonly string authServerAddress;
        private readonly string dataServerAddress;
        private readonly string certName;
        private const string frameApiUrl = "/api/frames";
        private const string authApiUrl = "/api/auth";
        private Timer tokenTimer;
        public OverseerNetworkClient(string authServer, string dataServer)
        {
            this.authServerAddress = authServer;
            this.dataServerAddress = dataServer;
            framesHttpClient = new HttpClient();
            framesHttpClient.BaseAddress = new Uri(dataServer);
            framesHttpClient.DefaultRequestHeaders.ConnectionClose = false;
            framesHttpClient.DefaultRequestHeaders.Connection.Add("Keep-alive");

            tokenTimer = new Timer(refreshToken, null, Timeout.Infinite, Timeout.Infinite);
        }

        ~OverseerNetworkClient()
        {
            Dispose();
        }

        private void refreshToken(object state)
        {
            Authenticate();
        }

        //regarding cert - read this
        //https://stackoverflow.com/questions/35582396/how-to-use-a-client-certificate-to-authenticate-and-authorize-in-a-web-api
        //https://community.sophos.com/kb/en-us/132438
        /// <summary>
        /// Don't forget to dispose this bad boy when not needed anymore
        /// </summary>
        /// <param name="serverAddress">ip, url, FQDN. whatever</param>
        /// <param name="certName">friendly certificate name as it is stored in personal storage</param>
        public OverseerNetworkClient(string authServer, string dataServer, string certName):this(authServer, dataServer)
        {
            this.certName = certName;
        }


        private static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None)
                return true;

            // Do not allow this client to communicate with unauthenticated servers.
            return false;
        }

        public void Test()
        {
            HttpResponseMessage hrp = this.framesHttpClient.PostAsync("/api/test/133", null).Result;
            var a = hrp.StatusCode;
        }

        public bool Authenticate()
        {
            WebRequestHandler handler = new WebRequestHandler();
            X509Certificate certificate = getCert(certName);
            handler.ClientCertificates.Add(certificate);
            handler.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateServerCertificate);
            handler.ClientCertificateOptions = ClientCertificateOption.Manual;
            handler.PreAuthenticate = true;

            using (HttpClient authHttpClient = new HttpClient(handler, true))
            {
                HttpResponseMessage hrp = authHttpClient.PostAsync(authServerAddress + "api/auth", null).Result;

                OverseerToken token = JsonConvert.DeserializeObject<OverseerToken>(hrp.Content.ReadAsStringAsync().Result);
                updateToken(token.AccessToken);
                resetTimer(token.LifetimeSeconds);
                return hrp.IsSuccessStatusCode;
            }
        }

        private void resetTimer(int tokenLifetime)
        {
            this.tokenTimer.Change(tokenLifetime * 900, Timeout.Infinite);
        }

        private void updateToken(string jwtToken)
        {
            this.framesHttpClient.DefaultRequestHeaders.Remove("Authorization");
            this.framesHttpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + jwtToken);
        }

        /// <summary>
        /// Send new frame
        /// </summary>
        /// <param name="data">dx shit</param>
        /// <param name="sourceId">monitor id or whatever</param>
        /// <returns>server's response</returns>
        public HttpStatusCode SendNewFrame(byte[] data, string sourceId) => sendMessage(new HttpRequestMessage(HttpMethod.Post, frameApiUrl) { Content = new StringContent(JsonConvert.SerializeObject(prepareNewFrame(data, sourceId)), Encoding.UTF8, "application/json") });

        /// <summary>
        /// If frame hasn't changed use this
        /// </summary>
        /// <param name="sourceId">monitor id or whatever</param>
        /// <returns>server's response</returns>
        public HttpStatusCode SendStaticFrame(string sourceId) => sendMessage(new HttpRequestMessage(HttpMethod.Post, frameApiUrl) { Content = new StringContent(JsonConvert.SerializeObject(prepareStaticFrame(sourceId)), Encoding.UTF8, "application/json") });

        /// <summary>
        /// Notify server that the current session is over. You probably wanna call Dispose after that
        /// </summary>
        /// <returns>server's response</returns>
        public HttpStatusCode StopTransmission() => sendMessage(new HttpRequestMessage(HttpMethod.Post, frameApiUrl) { Content = new StringContent(JsonConvert.SerializeObject(prepareStopFrame())) });

        private HttpStatusCode sendMessage(HttpRequestMessage hrq) 
        {
            bool firstTime = true;
            HttpResponseMessage hrp = null;
            do
            {
                hrp = this.framesHttpClient.SendAsync(hrq).Result;
                if (hrp.StatusCode == HttpStatusCode.Unauthorized || hrp.StatusCode == HttpStatusCode.Forbidden)
                {
                    Authenticate();
                    firstTime = false;
                }
            } while (firstTime);
            return hrp.StatusCode;
        }

        private Frame prepareNewFrame(byte[] data, string sourceId) => new Frame() { Control = FrameControl.New, Data = data, SourceId = sourceId };
        private Frame prepareStaticFrame(string sourceId) => new Frame() { Control = FrameControl.Static, SourceId = sourceId };
        private Frame prepareStopFrame() => new Frame() { Control = FrameControl.Stop };

        private static X509Certificate2 getCert(string certName)
        {
            X509Store myX509Store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            myX509Store.Open(OpenFlags.ReadOnly);
            X509Certificate2 myCertificate = myX509Store.Certificates.OfType<X509Certificate2>().FirstOrDefault(cert => cert.FriendlyName == certName);
            return myCertificate;
        }

        public void Dispose()
        {
            framesHttpClient.Dispose();
        }
    }
}
