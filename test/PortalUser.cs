﻿using System;

namespace OverseerDbContext
{

    //impractical when there are more then 64 roles, flagged enum just won't be able to contain them all
    [Flags]
    public enum Role: long
    {
        Regular = 0,
        Admin = 1
    }

    public class PortalUser
    {
        public int Id { get; set; }
        public string PasswordHash { get;set; }
        public Role Roles { get; set; }
        public string Name { get; set; }
    }
}