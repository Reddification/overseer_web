﻿using Microsoft.EntityFrameworkCore;
using System;

namespace OverseerDbContext
{
    public class OverseerDbContext:DbContext
    {
        public OverseerDbContext(DbContextOptions<OverseerDbContext> options): base(options)
        {
        }

        public OverseerDbContext()
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=helloappdb;Trusted_Connection=True;");
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseLazyLoadingProxies(); //using lazy loading is not always good but sooooo tempting
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Machine>().HasIndex(i => i.DomainName).IsUnique(true);
            modelBuilder.Entity<Client>().HasIndex(i => i.CertThumbprint).IsUnique(true);
            base.OnModelCreating(modelBuilder);//not sure if this one is still required
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<PortalUser> Users { get; set; }
        public DbSet<Client2Machine> Client2Machines { get; set; }
        public DbSet<Machine> Machines { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<OutputDevice> OutputDevices { get; set; }
    }
}
