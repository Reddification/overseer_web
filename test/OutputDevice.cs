﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OverseerDbContext
{
    /// <summary>
    /// These are devices adapters like nvidia cards or intel integrated gpus just to keep track of to which device which frame belongs to 
    /// </summary>
    public class OutputDevice
    {
        public int Id { get; set; }
        [ForeignKey("Machine")]
        public int MachineId { get; set; }
        public virtual Machine Machine { get; set; }
        //this is just an adapter name
        public string Name { get; set; }
        //file guid is used as a file name in the file system. path to files is a web app setting in appsettings.json
    }
}
