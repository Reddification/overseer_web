﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OverseerDbContext
{
    public class Session
    {
        public int Id { get; set; }
        [ForeignKey("Client2Machine")]
        public int Client2MachineId { get; set; }
        [ForeignKey("Device")]
        public int? DeviceId { get; set; }
        public virtual Client2Machine Client2Machine {get;set;}
        public virtual OutputDevice Device { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public Guid FileId { get; set; }

        [NotMapped]
        public TimeSpan Duration => (EndTime ?? DateTime.Now) - StartTime;
    }
}
