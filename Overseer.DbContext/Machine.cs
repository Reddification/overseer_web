﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Overseer.DbContext
{
    public class Machine
    {
        public int Id { get; set; }

        public string DomainName { get; set; }
        public string Description { get; set; }
        public virtual ICollection<OutputDevice> OutputDevices { get; set; }
        public virtual ICollection<Client2Machine> Clients { get; set; }
    }
}
