﻿using System.Collections.Generic;

namespace Overseer.DbContext
{
    public class Client
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Client2Machine> Machines { get; set; }
        //https://stackoverflow.com/questions/23986171/is-fingerprint-of-public-cert-unique
        public string CertThumbprint { get; set; }
    }
}