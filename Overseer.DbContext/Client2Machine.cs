﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Overseer.DbContext
{
    public class Client2Machine
    {
        public int Id { get; set; } //a bit redundant but whatever
        [ForeignKey("Client")]
        public int ClientId { get; set; }
        [ForeignKey("Machine")]
        public int MachineId { get; set; }
        public virtual Client Client { get; set; }
        public virtual Machine Machine { get; set; }
    }
}
